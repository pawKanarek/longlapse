using Android.Graphics;
using Android.Media;
using Java.IO;
using Java.Lang;
using Java.Nio;
using LongLapse.Helpers;
using System;

namespace LongLapse.Listeners
{
    public class ImageAvailableListener : Java.Lang.Object, ImageReader.IOnImageAvailableListener
    {
        private readonly File _file;
        private readonly LongLapseFragment _longLapseFragment;

        public ImageAvailableListener(LongLapseFragment longLapseFragment, File file)
        {
            _longLapseFragment = longLapseFragment ?? throw new System.ArgumentNullException("longLapseFragment");
            _file = file ?? throw new System.ArgumentNullException("file");
        }

        public void OnImageAvailable(ImageReader reader)
        {
            _longLapseFragment.BackgroundHandler.Post(new ImageSaver(reader.AcquireNextImage(), _file, _longLapseFragment.Activity.ContentResolver));
        }

        /// <summary>
        /// Saves a JPEG Image into the specified File.
        /// </summary>
        private class ImageSaver : Java.Lang.Object, IRunnable
        {
            /// <summary>
            /// The JPEG image
            /// </summary>
            private Image _image;

            /// <summary>
            /// The file we save the image into.
            /// </summary>
            private File _file;
            private readonly Android.Content.ContentResolver _contentResolver;

            public ImageSaver(Image image, File file, Android.Content.ContentResolver contentResolver)
            {
                _image = image ?? throw new System.ArgumentNullException("image");
                _file = file ?? throw new System.ArgumentNullException("file");
                _contentResolver = contentResolver;
            }

            public void Run()
            {

                ByteBuffer buffer = _image.GetPlanes()[0].Buffer;
                byte[] bytes = new byte[buffer.Remaining()];
                buffer.Get(bytes);


                Bitmap bitmapImage = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length, null);

                CapturePhotoHelper.InsertImage(_contentResolver, bitmapImage, DateTimeOffset.Now.ToString(), DateTimeOffset.Now.ToString());

                
                using (var output = new FileOutputStream(_file))
                {
                    try
                    {
                        output.Write(bytes);
                    }
                    catch (IOException e)
                    {
                        e.PrintStackTrace();
                    }
                    finally
                    {
                        _image.Close();
                    }
                }
            }
        }
    }
}