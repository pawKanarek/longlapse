using Android.Hardware.Camera2;
using LongLapse.Helpers;

namespace LongLapse.Listeners
{
    public class CameraCaptureSessionCallback : CameraCaptureSession.StateCallback
    {
        private readonly LongLapseFragment _longLapseFragment;

        public CameraCaptureSessionCallback(LongLapseFragment longLapseFragment)
        {
            _longLapseFragment = longLapseFragment ?? throw new System.ArgumentNullException("longLapseFragment");
        }

        public override void OnConfigureFailed(CameraCaptureSession session)
        {
            ToastHelper.ShowToast(_longLapseFragment.Activity, "Failed");
        }

        public override void OnConfigured(CameraCaptureSession session)
        {
            // The camera is already closed
            if (_longLapseFragment.CameraDevice == null)
            {
                return;
            }

            // When the session is ready, we start displaying the preview.
            _longLapseFragment.CaptureSession = session;
            try
            {
                // Auto focus should be continuous for camera preview.
                _longLapseFragment.PreviewRequestBuilder.Set(CaptureRequest.ControlAfMode, (int)ControlAFMode.ContinuousPicture);
                // Flash is automatically enabled when necessary.
                _longLapseFragment.SetAutoFlash(_longLapseFragment.PreviewRequestBuilder);

                // Finally, we start displaying the camera preview.
                _longLapseFragment.PreviewRequest = _longLapseFragment.PreviewRequestBuilder.Build();
                _longLapseFragment.CaptureSession.SetRepeatingRequest(_longLapseFragment.PreviewRequest,
                        _longLapseFragment.CaptureCallback, _longLapseFragment.BackgroundHandler);
            }
            catch (CameraAccessException e)
            {
                e.PrintStackTrace();
            }
        }
    }
}