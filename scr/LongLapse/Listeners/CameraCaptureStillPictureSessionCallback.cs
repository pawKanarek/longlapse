using Android.Hardware.Camera2;
using Android.Util;
using LongLapse.Helpers;

namespace LongLapse.Listeners
{
    public class CameraCaptureStillPictureSessionCallback : CameraCaptureSession.CaptureCallback
    {
        private static readonly string TAG = "CameraCaptureStillPictureSessionCallback";

        private readonly LongLapseFragment _longLapseFragment;

        public CameraCaptureStillPictureSessionCallback(LongLapseFragment longLapseFragment)
        {
            _longLapseFragment = longLapseFragment ?? throw new System.ArgumentNullException("longLapseFragment");
        }

        public override void OnCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result)
        {
            // If something goes wrong with the save (or the handler isn't even 
            // registered, this code will toast a success message regardless...)
            ToastHelper.ShowToast(_longLapseFragment.Activity, "Saved: " + _longLapseFragment.File);
            Log.Debug(TAG, _longLapseFragment.File.ToString());
            _longLapseFragment.UnlockFocus();
        }
    }
}
