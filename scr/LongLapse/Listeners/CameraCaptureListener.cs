using Android.Hardware.Camera2;
using Java.IO;
using Java.Lang;
using LongLapse.Infrastructure;
using System;

namespace LongLapse.Listeners
{
    public class CameraCaptureListener : CameraCaptureSession.CaptureCallback
    {
        private readonly LongLapseFragment longLapseFragment;

        public CameraCaptureListener(LongLapseFragment owner)
        {
            this.longLapseFragment = owner ?? throw new System.ArgumentNullException("owner");
        }

        public override void OnCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result)
        {
            Process(result);
        }

        public override void OnCaptureProgressed(CameraCaptureSession session, CaptureRequest request, CaptureResult partialResult)
        {
            Process(partialResult);
        }

        private void Process(CaptureResult result)
        {
            switch (longLapseFragment.State)
            {
                case LongLapseConstants.STATE_WAITING_LOCK:
                    {
                        var afState = Convert.ToInt32(result.Get(CaptureResult.ControlAfState));
                        if (afState == 0)
                        {
                            longLapseFragment.CaptureStillPicture();
                        }
                        else if ((int)ControlAFState.FocusedLocked == afState || (int)ControlAFState.NotFocusedLocked == afState)
                        {
                            // ControlAeState can be null on some devices
                            var aeState = Convert.ToInt32(result.Get(CaptureResult.ControlAeState));
                            if (aeState == (int)ControlAEState.Inactive || aeState == (int)ControlAEState.Converged)
                            {
                                longLapseFragment.State = LongLapseConstants.STATE_PICTURE_TAKEN;
                                longLapseFragment.CaptureStillPicture();
                            }
                            else
                            {
                                longLapseFragment.RunPrecaptureSequence();
                            }
                        }
                        break;
                    }
                case LongLapseConstants.STATE_WAITING_PRECAPTURE:
                    {
                        // ControlAeState can be null on some devices
                        var aeState = Convert.ToInt32(result.Get(CaptureResult.ControlAeState));
                        if (aeState == (int)ControlAEState.Inactive
                            || aeState == ((int)ControlAEState.Precapture) || aeState == ((int)ControlAEState.FlashRequired))
                        {
                            longLapseFragment.State = LongLapseConstants.STATE_WAITING_NON_PRECAPTURE;
                        }
                        break;
                    }
                case LongLapseConstants.STATE_WAITING_NON_PRECAPTURE:
                    {
                        // ControlAeState can be null on some devices
                        var aeState = Convert.ToInt32(result.Get(CaptureResult.ControlAeState));
                        if (aeState == (int)ControlAEState.Inactive || aeState != (int)ControlAEState.Precapture)
                        {
                            longLapseFragment.State = LongLapseConstants.STATE_PICTURE_TAKEN;
                            longLapseFragment.CaptureStillPicture();
                        }
                        break;
                    }
            }
        }
    }
}