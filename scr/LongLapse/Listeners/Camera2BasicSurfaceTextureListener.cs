using Android.Views;

namespace LongLapse.Listeners
{
    public class LongLapseSurfaceTextureListener : Java.Lang.Object, TextureView.ISurfaceTextureListener
    {
        private readonly LongLapseFragment _longlapseFragment;

        public LongLapseSurfaceTextureListener(LongLapseFragment longLapseFragment)
        {
            _longlapseFragment = longLapseFragment ?? throw new System.ArgumentNullException("longLapseFragment");
        }

        public void OnSurfaceTextureAvailable(Android.Graphics.SurfaceTexture surface, int width, int height)
        {
            _longlapseFragment.OpenCamera(width, height);
        }

        public bool OnSurfaceTextureDestroyed(Android.Graphics.SurfaceTexture surface)
        {
            return true;
        }

        public void OnSurfaceTextureSizeChanged(Android.Graphics.SurfaceTexture surface, int width, int height)
        {
            _longlapseFragment.ConfigureTransform(width, height);
        }

        public void OnSurfaceTextureUpdated(Android.Graphics.SurfaceTexture surface)
        {

        }
    }
}