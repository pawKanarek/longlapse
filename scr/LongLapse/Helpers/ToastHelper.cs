﻿using Android.App;
using Android.Content;
using Android.Widget;
using Java.Lang;

namespace LongLapse.Helpers
{
    public class ToastHelper
    {
        // Shows a {@link Toast} on the UI thread.
        public static void ShowToast(Activity activity, string text)
        {
            if (activity != null)
            {
                activity.RunOnUiThread(new ShowToastRunnable(activity.ApplicationContext, text));
            }
        }

        private class ShowToastRunnable : Object, IRunnable
        {
            private readonly string text;
            private readonly Context context;

            public ShowToastRunnable(Context context, string text)
            {
                this.context = context;
                this.text = text;
            }

            public void Run()
            {
                Toast.MakeText(context, text, ToastLength.Short).Show();
            }
        }

    }
}