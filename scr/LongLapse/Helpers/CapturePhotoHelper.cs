﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using static Android.Provider.MediaStore;

namespace LongLapse.Helpers
{

    public static class CapturePhotoHelper
    {

        /// <summary>
        /// A copy of the Android internals  insertImage method, this method populates the 
        /// meta data with DATE_ADDED and DATE_TAKEN.This fixes a common problem where media 
        /// that is inserted manually gets saved at the end of the gallery (because date is not populated).
        /// </summary>
        /// <param name="cr">Content resolver obtained from Activity</param>
        /// <param name="source">Bitmap source</param>
        /// <param name="title">Image title</param>
        /// <param name="description">image description</param>
        /// <returns>Url where photo was saved</returns>
        public static String InsertImage(ContentResolver cr, Bitmap source, String title, String description)
        {
            ContentValues values = new ContentValues();
            values.Put(Images.Media.InterfaceConsts.Title, title);
            values.Put(Images.Media.InterfaceConsts.DisplayName, title);
            values.Put(Images.Media.InterfaceConsts.Description, description);
            values.Put(Images.Media.InterfaceConsts.MimeType, "image/jpeg");
            // Add the date meta data to ensure the image is added at the front of the gallery
            values.Put(Images.Media.InterfaceConsts.DateAdded, DateTimeOffset.Now.TimeOfDay.TotalMilliseconds);
            values.Put(Images.Media.InterfaceConsts.DateTaken, DateTimeOffset.Now.TimeOfDay.TotalMilliseconds);

            Android.Net.Uri url = null;
            String stringUrl = null;    /* value to be returned */

            try
            {
                Android.Net.Uri uri = Android.Net.Uri.Parse(MediaStore.Images.Media.ExternalContentUri.ToString());
                url = cr.Insert(uri, values);

                if (source != null)
                {
                    var imageOut = cr.OpenOutputStream(url);
                    try
                    {
                        source.Compress(Bitmap.CompressFormat.Jpeg, 50, imageOut);
                    }
                    finally
                    {
                        imageOut.Close();
                    }

                    long id = ContentUris.ParseId(url);
                    // Wait until MINI_KIND thumbnail is generated.
                    Bitmap miniThumb = Images.Thumbnails.GetThumbnail(cr, id, ThumbnailKind.MiniKind, null);
                    // This is for backward compatibility.
                    StoreThumbnail(cr, miniThumb, id, 50F, 50F, ThumbnailKind.MicroKind);
                }
                else
                {
                    cr.Delete(url, null, null);
                    url = null;
                }
            }
            catch (Exception e)
            {
                if (url != null)
                {
                    cr.Delete(url, null, null);
                    url = null;
                }
            }

            if (url != null)
            {
                stringUrl = url.ToString();
            }

            return stringUrl;
            /**
     * 
     */
        }


        /// <summary>
        /// A copy of the Android internals StoreThumbnail method, it used with the insertImage to
        /// populate the android.provider.MediaStore.Images.Media#insertImage with all the correct
        /// meta data.The StoreThumbnail method is private so it must be duplicated here.
        /// @see android.provider.MediaStore.Images.Media (StoreThumbnail private method)
        /// </summary>
        /// <param name="cr">Content Resolver obtained from Activity</param>
        /// <param name="source">bitmap image source</param>
        /// <param name="id">id</param>
        /// <param name="width">width</param>
        /// <param name="height">height</param>
        /// <param name="thumbnailKind">Thumbial kind</param>
        /// <returns></returns>
        private static Bitmap StoreThumbnail(ContentResolver cr, Bitmap source, long id, float width, float height, ThumbnailKind thumbnailKind)
        {

            // create the matrix to scale it
            Matrix matrix = new Matrix();

            float scaleX = width / source.Width;
            float scaleY = height / source.Height;

            matrix.SetScale(scaleX, scaleY);

            Bitmap thumb = Bitmap.CreateBitmap(source, 0, 0, source.Width, source.Height, matrix, true);

            ContentValues values = new ContentValues(4);
            values.Put(Images.Thumbnails.Kind, (int)thumbnailKind);
            values.Put(Images.Thumbnails.ImageId, (int)id);
            values.Put(Images.Thumbnails.Height, thumb.Height);
            values.Put(Images.Thumbnails.Width, thumb.Width);

            Android.Net.Uri uri = Android.Net.Uri.Parse(Images.Thumbnails.ExternalContentUri.ToString());

            Android.Net.Uri url = cr.Insert(uri, values);

            try
            {
                var thumbOut = cr.OpenOutputStream(url);
                thumb.Compress(Bitmap.CompressFormat.Jpeg, 100, thumbOut);
                thumbOut.Close();
                return thumb;
            }
            catch (FileNotFoundException ex)
            {
                return null;
            }
            catch (IOException ex)
            {
                return null;
            }
        }
    }
}

