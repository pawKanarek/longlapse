
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V13.App;
using LongLapse.Infrastructure;

namespace LongLapse
{
    public class ConfirmationDialog : DialogFragment
    {
        private static Fragment _parent;

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            _parent = ParentFragment;
            return new AlertDialog.Builder(Activity)
                .SetMessage(Resource.String.request_permission)
                .SetPositiveButton(Android.Resource.String.Ok, new PositiveListener())
                .SetNegativeButton(Android.Resource.String.Cancel, new NegativeListener())
                .Create();
        }

        private class PositiveListener : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            public void OnClick(IDialogInterface dialog, int which)
            {
                FragmentCompat.RequestPermissions(_parent,
                    new string[] { Manifest.Permission.Camera, Manifest.Permission.WriteExternalStorage }, LongLapseConstants.REQUEST_CAMERA_PERMISSION);
            }
        }

        private class NegativeListener : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            public void OnClick(IDialogInterface dialog, int which)
            {
                Activity activity = _parent.Activity;
                if (activity != null)
                {
                    activity.Finish();
                }
            }
        }
    }
}