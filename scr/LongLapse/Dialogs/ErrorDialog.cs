
using Android.App;
using Android.Content;
using Android.OS;

namespace LongLapse
{
    public class ErrorDialog : DialogFragment
    {
        private static readonly string ARG_MESSAGE = "message";
        private static Activity _activity;

        public ErrorDialog(string message)
        {
            Arguments = new Bundle();
            Arguments.PutString(ARG_MESSAGE, message);
        }

        private class PositiveListener : Java.Lang.Object, IDialogInterfaceOnClickListener
        {
            public void OnClick(IDialogInterface dialog, int which)
            {
                _activity.Finish();
            }
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            _activity = Activity;
            return new AlertDialog.Builder(_activity)
                .SetMessage(Arguments.GetString(ARG_MESSAGE))
                .SetPositiveButton(Android.Resource.String.Ok, new PositiveListener())
                .Create();
        }
    }
}