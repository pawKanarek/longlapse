﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;

namespace LongLapse
{
	[Activity(Label = "@string/app_name",  MainLauncher = true)]
	public class MainActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_camera);

            if (savedInstanceState == null)
            {
                FragmentManager.BeginTransaction().Replace(Resource.Id.container, new LongLapseFragment()).Commit();
            }
        }
	}
}

